var ngModule = angular.module("contactList", ['ngMaterial']);

ngModule.controller("mainController", function($rootScope, $scope){
  var vm = this;

  $rootScope.appTitle = "Lista de Contatos";

  vm.redirect = redirect;
  vm.routes = {
    contacts: {
      route: 'contacts/contacts-view.html',
      controller: 'contactListCtrl'
    }
  };
  vm.route = vm.routes['contacts'];
  $scope.controller = vm.routes['contacts'].controller;

  function redirect(route){
    vm.route = vm.routes[route].route;
    vm.controller = vm.routes[route].controller;
  }

});
