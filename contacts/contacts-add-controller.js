angular.module("contactList").controller("contactAddCtrl", ["$scope", function($scope) {

  $scope.operators = [

    {name: "Vivo", value: "32.11"},
    {name: "Tim", value: "13.21"},
    {name: "Claro", value: "44.23"},
    {name: "Oi", value: "12.00"}
  ];

  $scope.insertContact = function(contact) {

    contact.isFavorite = false;
    contact.date = new Date();
    //$scope.contactList.push(angular.copy(contact));

    delete $scope.contact;
    $scope.formContact.$setPristine();
    $scope.addContact = false;
  }
}]);
